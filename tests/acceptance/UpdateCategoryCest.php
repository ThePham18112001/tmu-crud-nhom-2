<?php

use Page\Acceptance\CategoryPage;
use Step\Acceptance\CategoryStep;

class UpdateCategoryCest
{
    public function testcase_name_successfully(CategoryStep $categoryStep, CategoryPage $categoryPage)
    {
        $categoryStep->update('Nhom 2');
        $categoryPage->clickUpdateButton();
        $categoryStep->retrySee('Update category success.');
    }

    public function testcase_name_blank(CategoryStep $categoryStep, CategoryPage $categoryPage)
    {
        $categoryStep->update('');
        $categoryPage->clickUpdateButton();
        $categoryStep->retrySee('The name field is required.');
    }

    public function testcase_name_duplicated(CategoryStep $categoryStep, CategoryPage $categoryPage)
    {
        $categoryStep->update('Nhom 2');
        $categoryPage->clickUpdateButton();
        $categoryStep->retrySee('The name field is duplicated.');
    }

    public function testcase_name_is_spaces(CategoryStep $categoryStep, CategoryPage $categoryPage)
    {
        $categoryStep->update('  ');
        $categoryPage->clickUpdateButton();
        $categoryStep->retrySee('The name field is invalid.');
    }

    public function testcase_name_is_html_and_js(CategoryStep $categoryStep, CategoryPage $categoryPage)
    {
        $categoryStep->update('<p>123</p>');
        $categoryPage->clickUpdateButton();
        $categoryStep->retrySee('The name field is invalid.');
    }
}
