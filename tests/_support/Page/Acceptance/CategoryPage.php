<?php
namespace Page\Acceptance;

class CategoryPage
{
    // include url of current page
    public static $URL = '/categories';

    const NAME_FIELD = '//input[@id ="name"]';
    const ADD_BUTTON = '//button[@id="add-btn"]';
    const UPDATE_BUTTON = '//button[@id="update-btn"]';
    const EDIT_BUTTON = '//button[@class="btn btn-primary mr-2"]';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    /**
     * @var \AcceptanceTester;
     */
    protected $acceptanceTester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->acceptanceTester = $I;
    }

    public function onPage(){
        $this->acceptanceTester->amOnPage(self::$URL);
        return $this;
    }

    public function see($title){
        $this->acceptanceTester->see($title);
        return $this;
    }

    public function fillName($name){
        $this->acceptanceTester->retryFillField(self::NAME_FIELD, $name);
        return $this;
    }

    public function clickAddButton(){
        $this->acceptanceTester->retryClick(self::ADD_BUTTON);
        return $this;
    }

    public function clickEditButton(){
        $this->acceptanceTester->retryClick(self::EDIT_BUTTON);
        return $this;
    }

    public function clickUpdateButton(){
        $this->acceptanceTester->retryClick(self::ADD_BUTTON);
        return $this;
    }
}
