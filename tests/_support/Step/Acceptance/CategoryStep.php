<?php
namespace Step\Acceptance;

use Page\Acceptance\CategoryPage;

class CategoryStep extends \AcceptanceTester
{
    public function update($name){
        $categoryPage = new CategoryPage($this);
        $categoryPage->onPage()
            ->see('Update Category')
            ->clickEditButton()
            ->fillName($name);
    }
}